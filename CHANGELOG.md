# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html)
after the 1.0.0 release. Before this release, we're in full disaster hack mode.

## [Unreleased]

## [0.0.17] - 2018-11-08

### Removed

* `apc link`. It's a pain to manage, let's just skip this.

## [0.0.16] - 2018-11-08

### Added

* `apc link`: Link settings files for a Django project command.

## [0.0.15] - 2018-11-08

### Fixed

* Inability to determine `env` when deploying and failing to load the Ansible vault.
  * See https://git.coop/aptivate/internal-tools/aptivate-cli/issues/10.

### Removed

* Outreach metrics stubs. We're experimenting with that somewhere else now.
  * See https://git.coop/aptivate/internal-tools/aptivate-cli/issues/7.

* `ansible-runner` dependency. We can shell out to Ansible and it is easier.
  * Then we also don't have to install Ansible which is left to the project dependency.
  * See https://git.coop/aptivate/internal-tools/aptivate-cli/issues/9.

* `apc scenario` command. We don't strictly need this and want to keep this tool lean.
* `apc run` command. We don't strictly need this and want to keep this tool lean.

## [0.0.14] - 2018-10-18

## Added

* `apc run` command.

## Removed

* Free arguments being passed the root `apc`.

## [0.0.13] - 2018-10-18

## Added

* `apc login` command.
* `apc shell` command.
* `apc load` command.
* `apc <any-django-command>` runner.
* `apc scenario` command.
* Message to show how to skip prompts.
* Command reference documentation.

## [0.0.12] - 2018-10-15

## Changed

* Only check for password store when running stage/prod deployments.
* Only configure vault password when running stage/prod deployments.
* Allow prompts to accept empty values.

## [0.0.11] - 2018-10-15

## Added

* The `--no-input` flag for CI deployments.
* `aptivate_cli.config.Config` checks based on `$GITLAB_CI` environment variable.
* Testing for Python version 3.5.

## Changed

* Environment variable naming to be more clear. See the `README.md` for more.
* Releases are now `bdist_wheel` format.
* `apc isort` gets some baked in default arguments.

## [0.0.10] - 2018-10-12

## Fixed

* Circular import errors related to MyPy.

## [0.0.9] - 2018-10-12

## Fixed

* Missing `__init__.py` for the `cligroups` module.

## [0.0.8] - 2018-10-12

## Removed

* Magic installation script.

## Added

* Outreach metric placeholder commands.
* Gitlab CI placeholder commands.
* Ability to read from the password-store.
* Kanbantool API wrapper.
* Testing to support Python 3.5+ onwards.
* Gitlab CI command implementation.

## [0.0.7] - 2018-10-05

## Changed

* APTIVATE_PASSWORD... is now APTIVATE_CLI_PASS_DIR to match the rest.

## [0.0.6] - 2018-10-05

## Added

* Pyenv based installation script.
* Mandatory environment variable sanity checks.

## [0.0.5] - 2018-10-04

## Fixed

* `README.md` formatting and links.

## [0.0.4] - 2018-10-04

## Changed

* Switch URLs for `aptivate_cli.settings` to HTTPS.

## [0.0.3] - 2018-10-03

### Added

* Allow stage/prod deployments to run.

## [0.0.2] - 2018-10-03

### Fixed

* Allow `apc template -t role` to expose the prompts from cookiecutter.

## [0.0.1] - 2018-10-02

* Initial release.
