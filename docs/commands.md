# Command Reference

### A note on design

The design of `aptivate-cli` is based on conventions.

All conventions are followed by the [aptivate_cli.config.Config] object.

Each convention a command follows is noted in reference below.

[aptivate_cli.config.Config]: https://git.coop/aptivate/internal-tools/aptivate-cli/blob/master/aptivate_cli/config.py

## apc checks

Run Django `manage.py` sanity checks.

```bash
$ apc checks
```

## apc deploy

Deploy a Django application.

```bash
$ apc deploy -e {env}
```

Conventions:
  * Your current working directory name is the project name with matching:
    * Playbook project with format `{project-name}-play` on https://git.coop/aptivate/ansible-plays.
    * A matching `{env}` folder in the playbooks folder of the project play with a `deploy.yml` file.

See [internewshid-play](https://git.coop/aptivate/ansible-plays/internewshid-play) for a working example.

## apc isort

Run [isort] checks.

[isort]: https://isort.readthedocs.io/en/latest/

```bash
$ apc isort
```

Conventions:
  * `isort` is installed in your `pipenv` environment.
  * You have a `setup.cfg` which has an Isort configuration.

## apc load

Run a `mysqldump` and load it locally.

```bash
$ apc load -e {env}
```

Conventions:
  * Your current working directory name is the name of the remote database.
  * Your current working directory name has a matching Linode machine with format:
    * `lin-{project-name}-{env}`: for example, lin-internewshid-stage.

## apc login

Log into a remote machine.

```bash
$ apc login -e {env}
```

Conventions:
  * Your current working directory name has a matching Linode machine with format:
    * `lin-{project-name}-{env}`: for example, lin-internewshid-stage.

## apc pylava

Run [pylava] checks.

[pylava]: https://github.com/pylava/pylava

```bash
$ apc pylava
```

Conventions:
  * `pylava` is installed in your `pipenv` environment.
  * You have a `setup.cfg` which has a Pylava configuration.

## apc pytest

Run the [pytest] test suite.

[pytest]: https://docs.pytest.org/en/latest/contents.html

```bash
$ apc pytest
```

Conventions:
  * `pytest` is installed in your `pipenv` environment.
  * You have a `setup.cfg` which has a Pytest configuration.

## apc shell

Run a Django `manage.py` shell on a remote machine.

```bash
$ apc shell -e {env}
```

Conventions:
  * Your current working directory name has a matching Linode machine with format:
    * `lin-{project-name}-{env}`: for example, lin-internewshid-stage.

## apc template

Create a new project based on an Aptivate template.

```bash
$ apc template -t {type}
```
