# Migrate from DYE to aptivate-cli

---

What follows is a rough guide for Django based projects.

It's not automatic. It might be different for each project.

Your mileage may vary. At least you have the general steps here.

---

If your project is `internewshid`:

* Move the `deploy/pip_packages.txt` to `requirements.txt` and run `pipenv install`.
* Try to sort your dependencies in the new `Pipfile` into `[package]/[dev-packages]`.
* Delete the `deploy` folder and the `requirements.txt`.
* Delete the `apache` folder (keep a copy of the templates, you'll need it later).
* Move `django/website/manage.py` to `manage.py`.
* Move `django/website/LICENSE.txt` to `LICENSE.txt`.
* Move `django/website` contents to a new folder called `internewshid`.
* Delete the `django/website/` folder.
* Move `wsgi/wsgi_handler.py` to `wsgi.py` in the root path.
* Delete the `wsgi/` folder/.
* Add a `sys.path.append(os.path.join(os.path.dirname(__file__), 'internewshid'))` to your `manage.py` and `wsgi.py` file.
* Add a `os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')` to your `manage.py` and `wsgi.py` file.
* Keep hacking until you can run a `pipenv run python manage.py check`.
* Configure your `internewshid/local_settings.dev` to [look like this].
* Add a default Gitlab CI in the `.gitlab-ci.yml` file [with something like this].
* Create a matching `internewshid-play` project [over here] and try to match conventions for [the `apc deploy` command].
* Take our apache templates and put them in the Ansible plays.

That's it!

[look like this]: https://git.coop/aptivate/client-projects/internewshid/blob/d09c886193fd22d9c9df9299ebada254d64b43e2/internewshid/local_settings.py.dev#L12-26
[with something like this]: https://git.coop/aptivate/client-projects/internewshid/blob/d09c886193fd22d9c9df9299ebada254/.gitlab-ci.yml
[the `apc deploy` command]: https://git.coop/aptivate/internal-tools/aptivate-cli/blob/master/docs/commands.md#apc-deploy
[over here]: https://git.coop/aptivate/ansible-plays
