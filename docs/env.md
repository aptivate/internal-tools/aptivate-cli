## Environment Variables

You can configure these environment variables so that the tool can
automatically read configuration values and not prompt you for them on each
invocation.

* `APTIVATE_CLI_PASS_DIR`: Aptivate password store path.
* `APTIVATE_CLI_MYSQL_PASS`: MySQL root password.
* `APTIVATE_CLI_SUDO_PASS`: Sudo password.
* `APTIVATE_CLI_SSH_USER`: Your Aptivate SSH username.
