# Cheatsheet Reference

A handy command reference for moving to `aptivate-cli` from `DYE`.

See [docs/commands.md](docs/commands.md) for more information on specific commands.

| Task                             | DYE                                     | aptivate-cli                          |
| ---                              | ---                                     | ---                                   |
| Location of virtualenv           | `django/website/.ve`                    | `pipenv --venv` or use `$WORKON_HOME` |
| Add pip package to virtualenv    | Add to `deploy/pip_packages.txt`        | `pipenv install <package>`            |
| Deployment location on server    | `/var/django/<project>`                 | `/var/<project>/<project>`            |
| Deploy local environment         | `python deploy/tasks.py deploy:dev`     | `apc deploy`                          |
| Deploy to staging server         | `python deploy/fab.py staging deploy`   | `apc deploy -e stage`                 |
| Load a staging DB dump           | `python deploy/fab.py staging get...`   | `apc load -e stage`                   |
| Run manage.py command locally    | `python django/website/manage.py <cmd>` | `pipenv run python manage.py <cmd>`   |
| Run manage.py command on staging | `python django/website/manage.py <cmd>` | `pipenv run python manage.py <cmd>`   |
| Run a Django shell on staging    | N/A                                     | `apc shell -e stage`                  |
| Log into the staging server      | N/A                                     | `apc login -e stage`                  |
| Run pytest tests                 | N/A                                     | `apc pytest`                          |
| Run pylava style checks          | N/A                                     | `apc pylava`                          |
| Run isort style checks           | N/A                                     | `apc isort`                           |
| Run Django sanity checks         | N/A                                     | `apc checks`                          |
