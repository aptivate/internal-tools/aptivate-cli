import subprocess
from os import listdir

import pytest

from aptivate_cli import cmd
from aptivate_cli.config import Config


def test_cmd_call_gives_return_codes():
    assert cmd.call('echo Hello, World') == 0


def test_cmd_call_gives_exceptions():
    with pytest.raises(subprocess.CalledProcessError) as exception:
        cmd.call('grep foo /dev/null')
    assert isinstance(exception.value.returncode, int)


def test_cmd_ansible_home(django_project):
    cmd.create_ansible_home(Config())
    assert '.ansible' in listdir('.')
