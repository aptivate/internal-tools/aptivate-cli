"""Text fixtures."""

import typing
from os import chdir, mkdir
from os.path import join
from tempfile import TemporaryDirectory

import pytest


@pytest.fixture
def django_project() -> typing.Generator[str, None, None]:
    """A mocked Django application project structure."""
    with TemporaryDirectory() as temp_dir:
        mkdir(join(temp_dir, 'testproject'))
        chdir(join(temp_dir, 'testproject'))
        open('manage.py', 'a').close()
        yield temp_dir
