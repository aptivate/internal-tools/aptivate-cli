from os import environ, getcwd, remove
from os.path import abspath, join

import click
import pytest

from aptivate_cli.config import Config
from aptivate_cli.settings import (
    ANSIBLE_PLAYBOOK_FILE, APTIVATE_CLI_MYSQL_PASS, APTIVATE_CLI_PASS_DIR
)


def test_config_fails_sanity_checks(django_project, monkeypatch):
    remove('manage.py')

    with pytest.raises(click.ClickException) as exception:
        config = Config()
        config.django_checks()

    assert "No 'manage.py' in current directory" in str(exception)

    monkeypatch.delenv(APTIVATE_CLI_PASS_DIR, raising=False)

    with pytest.raises(click.ClickException) as exception:
        config = Config()
        config.env_var_checks()

    assert 'exposed in environment' in str(exception)


def test_config_passes_sanity_checks(django_project):
    assert Config() is not None


def test_config_project_name(django_project):
    assert Config().project_name == 'testproject'


def test_config_bad_variable_mapping(django_project):
    with pytest.raises(click.ClickException):
        Config().prompt_for_values(['foobar'])


def test_config_read_from_environment(django_project, monkeypatch):
    monkeypatch.setitem(environ, 'APTIVATE_CLI_MYSQL_PASS', 'foobar')

    config = Config()
    env_vars = config.prompt_for_values(['mysql_pass'])

    assert (
        env_vars[APTIVATE_CLI_MYSQL_PASS] ==
        config.env_vars[APTIVATE_CLI_MYSQL_PASS]
    )


def test_config_project_play_path(django_project):
    assert Config().project_play_path == join(
        abspath(getcwd()), '.ansible/testproject-play'
    )


@pytest.mark.parametrize('env', (
    'dev', 'stage', 'prod',
))
def test_config_playbook_path(env, django_project):
    config = Config()
    assert config.playbook_path(env) == join(
        abspath(getcwd()),
        '.ansible/{}-play/'.format(config.project_name),
        'playbooks/{}/{}'.format(env, ANSIBLE_PLAYBOOK_FILE)
    )


def test_config_load_cli_params(monkeypatch):
    config = Config()

    assert config.no_input is False
    config.load_cli_params()
    assert config.no_input is False

    config.load_cli_params(no_input=True)
    assert config.no_input is True
